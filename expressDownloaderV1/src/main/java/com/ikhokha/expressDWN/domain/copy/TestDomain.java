package com.ikhokha.expressDWN.domain.copy;

import javax.persistence.Column;
import javax.persistence.Table;

@Table
public class TestDomain {
	
	@Column
	private Long id;	
	@Column
	private String name;
	@Column
	private String surname;
        @Column
	private String file_url;
	
	public Long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getSurname() {
		return surname;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
        public String getFileUrl() {
		return file_url;
	}
	
		

}
