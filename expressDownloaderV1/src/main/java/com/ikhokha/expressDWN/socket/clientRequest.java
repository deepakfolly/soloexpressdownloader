/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ikhokha.expressDWN.socket;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author folly
 */
public class clientRequest {

    private String devId;
    private String appVersion;
    private String modem;
    private String signalStrength;
    private String PACKETSIZE;
    private String devMod;
    private boolean validJson;

    public clientRequest(String clientJsonReq) {
        try {
            JSONObject json = new JSONObject(clientJsonReq.substring(2));
            this.PACKETSIZE = json.getString("PACKETSIZE");
            this.devId = json.getString("devId");
            this.devMod = json.getString("devMod");
            this.appVersion = json.getString("appVer");
            //this.signalStrength = json.getString("signalStrength");
            //this.modem = json.getString("modem");
            this.validJson = true;
            System.out.println("devMod " + this.devMod);
            System.out.println("appVer " + this.appVersion);
            //System.out.println("signalStrength " + this.signalStrength);
            //System.out.println("modem " + this.modem);
            System.out.println("devId " + this.devId);
                    
        } catch (JSONException e) {
            this.validJson = false;
            System.out.println("Error parsing Json clientRequest ******* " + e);
       }
    }
    public boolean getvalidJson()
    {
        return validJson;
    }
    public String getDevId() {
        return devId;
    }

    public void setDevId(String devId) {
        this.devId = devId;
    }

    public Integer getAppVersion() {
        return Integer.parseInt(appVersion);
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getModem() {
        return modem;
    }

    public void setModem(String modem) {
        this.modem = modem;
    }

    public String getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(String signalStrength) {
        this.signalStrength = signalStrength;
    }

    public Integer getPACKETSIZE() {
        return Integer.parseInt(PACKETSIZE);
    }

    public void setPACKETSIZE(String PACKETSIZE) {
        this.PACKETSIZE = PACKETSIZE;
    }

}
