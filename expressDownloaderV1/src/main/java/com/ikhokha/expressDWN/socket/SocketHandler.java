/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ikhokha.expressDWN.socket;

import com.ikhokha.expressDWN.service.DeviceService;
import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author folly
 */
public class SocketHandler extends Thread{

    private Socket clientSocket;
    //private BufferedReader in;
    private DataOutputStream out;
    static final int NEW_LOADER_VERSION = 1043;
    static final int NEW4GDEVICE_VERSION = 3000;
    
    private DeviceService deviceService;

    public SocketHandler(Socket socket, DeviceService deviceService) {
        this.clientSocket = socket;
        this.deviceService = deviceService;
    }

    public void run() {

        try {
            int chunkSize = 0;
            Boolean is4G = false;
            clientSocket.setSoTimeout(30 * 1000);

            InputStreamReader isr = new InputStreamReader(clientSocket.getInputStream());
            out = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader fromclient = new BufferedReader(isr);
            DataOutputStream toClient = new DataOutputStream(clientSocket.getOutputStream());

            String clientSentence;
            clientSentence = fromclient.readLine();
            clientRequest clientReq = new clientRequest(clientSentence);
            if (clientReq.getvalidJson() == true)
            {
                System.out.println("Incoming client request " + clientSentence);
                System.out.println("Appversion is "+ clientReq.getAppVersion() + "packetsize" + clientReq.getPACKETSIZE());
                
                if (clientReq.getAppVersion() > NEW4GDEVICE_VERSION)
                {
                    System.out.println("This is a 4G device ");
                    is4G=true;
                }
                        
                try {
                    File jfile = new File(getFileUrlFromDB(clientReq.getDevId(),is4G));
                    if (!jfile.exists() || !jfile.isFile()) {
                        System.out.println("No valid file on server");
                    } else {
                        System.out.println("Is a valid file from DB");
                    }
                        //removed functionality for fast loader
                    if ((clientReq.getAppVersion() >= NEW_LOADER_VERSION) && (clientReq.getPACKETSIZE() > (1024 * 3))) {
                        System.out.println("Problematic fast loader selected");
                        System.out.println("Let's terminate the connection");
                        
                    } else {
                        System.out.println("Old loader selected");
                        slowLoader slowLoad = new slowLoader(jfile, toClient, clientReq.getPACKETSIZE(), fromclient,clientReq);
                        slowLoad.load();
                    }

                } catch (Exception e) {
                    System.out.println("NULL ******* " + e);
                }

                
                
            }
            else
            {
                System.out.println("Not a valid json");
            }

        } catch (IOException e) {
            System.out.println("IOException**** ");
            System.out.println(e);
        }

        try {
            System.out.println("Let's close the socket");
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void getDeviceIDFromDB(String serial_no) {
        try {
            System.out.println("*** Data from DB: " + deviceService.getBySerialNumber(serial_no,false));
        } catch (Exception e) {
        }
    }

    public String getFileUrlFromDB(String serial_no, Boolean is4G) {
        String fileURL = null;
        try {
            System.out.println("*** File URL from DB: " + deviceService.getBySerialNumber(serial_no,is4G));
            fileURL = deviceService.getBySerialNumber(serial_no,is4G);
        } catch (Exception e) {
            System.out.println("my exception" + e.getCause());
        }
        return fileURL;
    }
}
