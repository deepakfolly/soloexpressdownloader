/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ikhokha.expressDWN.socket;
//import com.ikhokha.expressDWN.persistence.domain.User;
import com.ikhokha.expressDWN.service.DeviceService;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 *
 * @author folly
 */

//type":"REQ","func":"GET_PING","devId":"001920900074","devMod":"PMSCR63G2WPRKB","appVer":"1041","sig
//nalStrength":"0052","modem":"Wifi","binlistVer":"000000000000""+ "
@Service
	@Configurable
	public class SocketServer {
		int port = 5000;

		@Autowired
		DeviceService deviceService;

		public SocketServer() {
		}

		@PostConstruct
		public void runSocketServer() {

			try (ServerSocket serverSocket = new ServerSocket(port)) {

				System.out.println("Server Version 3 is listening on port " + port);

				while (true) {
					Socket sSocket = serverSocket.accept();
					new SocketHandler(sSocket, deviceService).start();
				}

			} catch (IOException ex) {
				System.out.println("Server exception*** : " + ex.getMessage());
				ex.printStackTrace();
			}

		}
	}