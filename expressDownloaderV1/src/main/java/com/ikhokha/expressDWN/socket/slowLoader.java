/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ikhokha.expressDWN.socket;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 *
 * @author folly
 */
public class slowLoader {

    private File soloBinImage;
    private DataOutputStream clientSocket;
    private BufferedReader fromclient;
    private Integer chunkSize;
    private clientRequest clientRequest_;

    slowLoader(File jfile, DataOutputStream toClient, Integer binChunkSz, BufferedReader frmClient, clientRequest clientReq) {
        this.soloBinImage = jfile;
        this.clientSocket = toClient;
        this.chunkSize = binChunkSz;
        this.fromclient = frmClient;
        this.clientRequest_ = clientReq;
    }

    void load() {
        try {
            //Initialize all of the variables to spaces.
            byte[] padding = new byte[chunkSize];

            int filesize = (int) this.soloBinImage.length();
            int byteposition = 0;
            int bytestowrite = chunkSize;
            System.out.println(filesize);

            /*  step 2 [S2C] */
            this.clientSocket.writeShort(4); // length of the length of payload
            this.clientSocket.flush();
            this.clientSocket.writeInt(filesize); // length of payload
            this.clientSocket.flush();
            //Thread.sleep(10*1000);

            String clientSentence = this.fromclient.readLine(); //Step 3 [C2S]
            System.out.println(clientSentence);

            if (filesize > (640 * 1024)) {
                System.out.println("filesize too large ");
            }
            DataInputStream fis = new DataInputStream(new FileInputStream(this.soloBinImage));
            byte[] buffer = new byte[chunkSize];
            {
                while (fis.read(buffer) != -1)// reads CHUNKSIZE at a time
                {
                    if (byteposition + chunkSize > filesize) {
                        bytestowrite = filesize - byteposition;
                    } else {
                        bytestowrite = chunkSize;
                    }
                    System.out.println(
                            "before writing bytestowrite = " + bytestowrite + " byteposition " + byteposition);

                    /* step 4 [C2S]	 */
                    this.clientSocket.writeShort(bytestowrite); //len of chunksize pkt 
                    this.clientSocket.write(buffer, 0, bytestowrite);// chunksize pkt

                    /**
                     * ******************************************************************************
                     */
                    if (bytestowrite < chunkSize) {
                        System.out.println("Last write padding to socket size = " + (chunkSize - bytestowrite));
                        this.clientSocket.write(padding, 0, chunkSize - bytestowrite);// chunksize pkt
                        System.out.println("Download complete for " + clientRequest_.getDevId() + " " + clientRequest_.getAppVersion() + " SLOW loader");
                        Thread.sleep(10 * 1000);
                        break;
                    }
                    System.out.println("after writing, waiting for ack");
                    /* step 5 [c2S]  */
                    clientSentence = this.fromclient.readLine();
                    if (clientSentence.contains("ACK")) {
                        System.out.println("received ack from client = " + clientSentence);
                    }

                    byteposition = byteposition + bytestowrite;
                    System.out.println("looping byteposition " + byteposition);
                }

            }
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        } catch (Exception e) {
            System.out.println("SLOW LOADER NULL ******* " + e);
        }

    }

}
