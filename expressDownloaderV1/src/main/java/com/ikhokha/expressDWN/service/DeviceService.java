package com.ikhokha.expressDWN.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikhokha.expressDWN.persistence.domain.Device;
import com.ikhokha.expressDWN.persistence.dao.DeviceRepository;

@Service
public class DeviceService {
	
	@Autowired
	private DeviceRepository deviceRepository;

	public void saveMessage(Device device) {
		deviceRepository.save(device);
	}
	
	public List<Device> getDevices() {	
		System.out.print("The list: " + deviceRepository.findAll());
		return deviceRepository.findAll();
	}
	
	public Device getDeviceById(Long userId) {
		return deviceRepository.getOne(userId);
	}
	 
        public String getBySerialNumber(String serialNumber, Boolean is4G){
            
                if (isExist(serialNumber)) 
                {
                    Device device = deviceRepository.getBySerialNumber(serialNumber); 
                    System.out.println("Custome file for this device");
                    return device.getFileUrl();
                } 
                else 
                {
                    if (is4G)
                    {
                        Device device = deviceRepository.getBySerialNumber("default4G"); 
                        System.out.println("This is the default 4G file being used");
                        return device.getFileUrl();
                    }
                    else
                    {
                        Device device = deviceRepository.getBySerialNumber("default"); 
                        System.out.println("This is the default 3G file being used");
                        return device.getFileUrl();
                    }
                }
        }
        
        public boolean isExist(String SerialNo){
            //return true;
            return deviceRepository.existsBySerialNumber(SerialNo);
        }
	 
}
