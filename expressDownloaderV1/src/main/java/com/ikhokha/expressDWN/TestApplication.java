package com.ikhokha.expressDWN;

import com.ikhokha.expressDWN.socket.SocketServer;
//import com.ikhokha.test.socket.socketServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class TestApplication {

	public static void main(String[] args) {
		//SpringApplication.run(TestApplication.class, args);
                ConfigurableApplicationContext context =SpringApplication.run(TestApplication.class, args);
                context.getBean(SocketServer.class).runSocketServer();
                //socketServer Mysocket = new socketServer();
                //Mysocket.runSocketServer();
                
	}

}
