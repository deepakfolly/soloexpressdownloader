/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ikhokha.expressDWN.persistence.dao;
import com.ikhokha.expressDWN.persistence.domain.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
/**
 *
 * @author folly
 */
public interface DeviceRepository extends JpaRepository<Device, Long> {
    //@Query("FROM Device device WHERE device.serialNumber=?1")
    @Query("FROM Device device WHERE serial_number=?1")   
    Device getBySerialNumber(String serialNumber);
    //boolean existsBySerialNumber(String SerialNo);
    boolean existsBySerialNumber(String serialNumber);
}

