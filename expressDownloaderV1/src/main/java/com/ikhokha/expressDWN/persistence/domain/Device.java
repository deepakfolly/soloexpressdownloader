/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ikhokha.expressDWN.persistence.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author folly
 */
@Entity
@Table(name = "devices")
public class Device {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;
	@Column(name = "name")
	private String name;
	@Column(name = "email")
	private String email;
	@Column(name = "phone_number")
	private String phoneNumber;
        //@Column(name = "serialNumber")
        @Column(name = "serialNumber", unique = true, nullable = false, length = 55)
	private String serialNumber;
        @Column(name = "file_url")
	private String file_url;
	
        public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
        public void setSerialNumber(String serialNo) {
		this.serialNumber = serialNo;
	}
        public String getSerialNumber() {
		return this.serialNumber;
	}
        public String getFileUrl() {
		return file_url;
	}
	@Override
	public String toString() {
		return "Contact [id=" + id + ", name=" + name + ", email=" + email + "phoneNumber="
				+ phoneNumber + "]";
	}

}

